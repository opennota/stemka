// This program is free software: you can redistribute it and/or modify it
// under the terms of the GNU General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General
// Public License for more details.
//
// You should have received a copy of the GNU General Public License along
// with this program.  If not, see <http://www.gnu.org/licenses/>.

// Package stemka provides a Russian language stemmer.
package stemka

import "sort"

var utf8 = [256]rune{
	0x2d: '-',
	0xe0: 'а',
	0xe1: 'б',
	0xe2: 'в',
	0xe3: 'г',
	0xe4: 'д',
	0xe5: 'е',
	0xe6: 'ж',
	0xe7: 'з',
	0xe8: 'и',
	0xe9: 'й',
	0xea: 'к',
	0xeb: 'л',
	0xec: 'м',
	0xed: 'н',
	0xee: 'о',
	0xef: 'п',
	0xf0: 'р',
	0xf1: 'с',
	0xf2: 'т',
	0xf3: 'у',
	0xf4: 'ф',
	0xf5: 'х',
	0xf6: 'ц',
	0xf7: 'ч',
	0xf8: 'ш',
	0xf9: 'щ',
	0xfa: 'ъ',
	0xfb: 'ы',
	0xfc: 'ь',
	0xfd: 'э',
	0xfe: 'ю',
	0xff: 'я',
}

func min(a, b int) int {
	if a < b {
		return a
	}
	return b
}

func vowel(r rune) bool {
	return r == 'а' || r == 'е' || r == 'и' || r == 'о' || r == 'у' ||
		r == 'ы' || r == 'э' || r == 'ю' || r == 'я'
}

func minStemLen(rr []rune) int {
	for i := 0; i < len(rr); i++ {
		if !vowel(rr[i]) {
			continue
		}
		for {
			i++
			if i >= len(rr) || !vowel(rr[i]) {
				break
			}
		}
		return i + 1
	}
	return len(rr)
}

// MinStem returns the minimal of the probable stems of s.
// s should contain only lowercase Russian letters а-я and (possibly) a dash.
// The letter `е` should be used instead of `ё`.
func MinStem(s string) string {
	index := 0
	rr := []rune(s)
	st := len(rr)
	m := minStemLen(rr)
outer:
	for i := len(rr) - 1; i >= m-3; i-- {
		n := int(data[index])
		for j := 0; j < n; j++ {
			r := utf8[data[index+j+1]]
			if r == 0 {
				st = i + 3
			} else if i >= 0 && r == rr[i] {
				k := index + n + j*2 + 1
				index = (int(data[k+1])<<8 + int(data[k])) * 8
				continue outer
			}
		}
		break
	}
	return string(rr[:st])
}

// StemLen returns a slice of probable s stem lengths.
// s should contain only lowercase Russian letters а-я and (possibly) a dash.
// The letter `е` should be used instead of `ё`.
// The code is mostly the same as in MinStem,
// but the stem lengths are appended to a slice.
func StemLen(s string) []int {
	index := 0
	rr := []rune(s)
	var st []int
	m := minStemLen(rr)
outer:
	for i := len(rr) - 1; i >= m-3; i-- {
		n := int(data[index])
		for j := 0; j < n; j++ {
			r := utf8[data[index+j+1]]
			if r == 0 {
				st = append(st, i+3)
			} else if i >= 0 && r == rr[i] {
				k := index + n + j*2 + 1
				index = (int(data[k+1])<<8 + int(data[k])) * 8
				continue outer
			}
		}
		break
	}
	sort.Ints(st)
	return st[:min(5, len(st))]
}
