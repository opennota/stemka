stemka [![License](http://img.shields.io/:license-gpl3-blue.svg)](http://www.gnu.org/licenses/gpl-3.0.html) [![GoDoc](https://godoc.org/gitlab.com/opennota/stemka?status.svg)](http://godoc.org/gitlab.com/opennota/stemka) [![Coverage report](https://gitlab.com/opennota/stemka/badges/master/coverage.svg)](https://gitlab.com/opennota/stemka/-/commits/master) [![Pipeline status](https://gitlab.com/opennota/stemka/badges/master/pipeline.svg)](https://gitlab.com/opennota/stemka/commits/master)
======

opennota/stemka is a Go port of the [stemka](https://sourceforge.net/projects/stemka/) stemmer for the Russian language.

## Install

    go get -u gitlab.com/opennota/stemka

